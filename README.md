# chef-pipelines

Chef Pipeline configs for Gitlab CI

## chef_cookbook_code_pipelines.yml

This file is for linting and testing all chef cookbooks and publishing to a chef-supermarket internally or community supermarket

## chef_repo_code_pipelines.yml

This file is for linting and deploying data_pages and policyfiles to a chef-server
